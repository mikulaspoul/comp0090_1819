#  #
# University College London COMP0090 for the 2018/19 term #

This repository contains course materials from the second half of the course.

All code is written in [Julia](https://julialang.org/), a modern scientific
programming language. An excellent introductory tutorial can be found
[here](https://julialang.org/learning/), as a language it is very similar to
a “modern” Matlab and Python.

1. [Download and install Julia](https://julialang.org/downloads/)
2. Start the Julia interpreter and run `using Pkg; Pkg.add("IJulia")` to install
    the Julia [Jupyter](https://jupyter.org/) bindings.
3. From the interpreter, run `using IJulia; notebook()` to launch Jupyter in
    your browser. Then navigate this directory to run the notebooks.
